* [SIASAR] DATA-ETL
1. Clone the project
2. Execute: npm install
3. Then: npm run build

4. Create database
5. Add the Postgis extension
6. Create and complete the file config.js

7. Execute npm run db:reset
