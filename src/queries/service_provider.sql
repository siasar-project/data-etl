SELECT
  t.prser_seq AS siasar_id,
  t.sep_a_001_001 AS name,
  t.sep_a_011_001 AS survey_date,
  p.pais_abreviatura AS adm_0,
  (
    WITH RECURSIVE adm as (
      SELECT tda_seq, tda_nome, tda_pai
      FROM tb_divisao_admin
      WHERE tda_seq = t.sep_a_002_001
      UNION ALL
      SELECT tda.tda_seq, tda.tda_nome, tda.tda_pai
      FROM tb_divisao_admin tda
      JOIN adm a ON tda.tda_seq = a.tda_pai
    )
    SELECT json_agg(tda_nome) FROM adm
  ) as adm,
  t.sep_c_004_001 AS month_billing,
  t.sep_a_006_001 AS longitude,
  t.sep_a_005_001 AS latitude,
  t.url_imagem AS picture_url,
  t.sep_g_001_001 AS preventive_maintenance,
  t.pais_sigla AS country,
  taxt1.taxt_nome as provider_type,
  taxt.taxt_nome as legal_status,
  t.classificacao as score,
  (
    SELECT COUNT(sep_b_008_001)
    FROM tb_dir_representante drep
    WHERE drep.prser_seq = t.prser_seq
      AND sep_b_008_001 = 32040
  ) AS women_count,
  (
    SELECT SUM(grpdo_num_domic_atend)
    FROM tb_grp_domicilio grpd
    WHERE grpd.prser_seq = t.prser_seq
  ) AS served_households,
  (
      SELECT json_object_agg(lower(v.varia_nome), vdw.vardw_valor)
      FROM tb_variavel_dw vdw
      JOIN tb_variavel v ON v.varia_seq = vdw.varia_seq
      WHERE vdw.vardw_entidade = t.prser_seq
      GROUP BY vardw_tempo
      ORDER BY vardw_tempo desc
      LIMIT 1
  ) AS variables
FROM tb_prest_servico t
LEFT JOIN tb_taxo_termo taxt on taxt.taxt_seq = t.sep_b_002_001
LEFT JOIN tb_grp_domicilio grpd on t.prser_seq = grpd.comun_seq
LEFT JOIN tb_taxo_termo taxt1 on taxt1.taxt_seq = t.sep_a_009_001
LEFT JOIN tb_pais p on p.pais_sigla = t.pais_sigla
ORDER BY t.prser_seq
