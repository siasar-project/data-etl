SELECT
  pat_seq as siasar_id,
  pat_a_005_001 as name,
  pat_a_002_001 AS survey_date,
  p.pais_abreviatura as adm_0,
  (
    WITH RECURSIVE adm as (
      SELECT tda_seq, tda_nome, tda_pai
      FROM tb_divisao_admin
      WHERE tda_seq = t.pat_a_007_001
      UNION ALL
      SELECT tda.tda_seq, tda.tda_nome, tda.tda_pai
      FROM tb_divisao_admin tda
      JOIN adm a ON tda.tda_seq = a.tda_pai
    )
    SELECT json_agg(tda_nome) FROM adm
  ) as adm,
  pat_a_010_001 as latitude,
  pat_a_011_001 as longitude,
  t.pat_a_004_001 AS picture_url,
  tt.taxt_nome as provider_type,
  t.pat_b_001_001 as served_households,
  p.pais_sigla as country,
  pat_c_001_001 AS technicians_count,
  pat_c_002_001 AS annual_budget,
  pat_c_006_001 AS transport_equipment,
  pat_c_008_001 AS equipment_measuring,
  pat_c_010_001 AS computer_equipment,
  pat_c_012_001 AS travel_expenses,
  pat_c_014_001 AS fuel,
  pat_c_016_001 AS internet_services,
  (
    SELECT json_build_object('value', t.pat_c_004_001, 'unit', u.unit_symbol)
    FROM tb_unit u
    WHERE u.unit_seq = t.pat_c_003_001
  ) AS budget,
  (
    SELECT json_object_agg(lower(v.varia_nome), vdw.vardw_valor)
    FROM tb_variavel_dw vdw
    JOIN tb_variavel v ON v.varia_seq = vdw.varia_seq
    WHERE vdw.vardw_entidade = t.pat_seq
    GROUP BY vardw_tempo
    ORDER BY vardw_tempo desc
    LIMIT 1
  ) AS variables
FROM tb_pat t
LEFT JOIN tb_pais p ON p.pais_sigla = t.pais_sigla
LEFT JOIN tb_taxo_termo tt ON t.pat_a_006_001 = tt.taxt_seq
