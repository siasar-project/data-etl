SELECT
  t.siste_seq AS siasar_id,
  t.sys_a_001_001 AS name,
  t.sys_a_031_001 AS survey_date,
  (
    WITH RECURSIVE adm as (
      SELECT tda_seq, tda_nome, tda_pai
      FROM tb_divisao_admin
      WHERE tda_seq = t.sys_a_004_001
      UNION ALL
      SELECT tda.tda_seq, tda.tda_nome, tda.tda_pai
      FROM tb_divisao_admin tda
      JOIN adm a ON tda.tda_seq = a.tda_pai
    )
    SELECT json_agg(tda_nome) FROM adm
  ) as adm,
  p.pais_abreviatura AS adm_0,
  t.sys_a_002_001 AS building_year,
  t.sys_a_008_001 AS longitude,
  t.sys_a_030_001 AS sketch_url,
  t.sys_a_034_001 AS picture_url,
  t.sys_a_007_001 AS latitude,
  t.pais_sigla AS country,
  t.classificacao as score,
  t.sys_g_003_001 as chlora,
  (
      SELECT json_object_agg(lower(v.varia_nome), vdw.vardw_valor)
      FROM tb_variavel_dw vdw
      JOIN tb_variavel v ON v.varia_seq = vdw.varia_seq
      WHERE vdw.vardw_entidade = t.siste_seq
      GROUP BY vardw_tempo
      ORDER BY vardw_tempo desc
      LIMIT 1
  ) AS variables,
  (
    SELECT array_agg(taxt.taxt_nome)
    FROM tb_tip_sist_abastecimento tpsa
    JOIN tb_taxo_termo taxt ON taxt.taxt_seq = tpsa.taxt_seq
    WHERE tpsa.siste_seq = t.siste_seq
  ) AS supply_type,
  (
    SELECT sum(rd.sys_f_003_001)
    FROM tb_rede_distribuicao rd
    WHERE rd.siste_seq = t.siste_seq
  ) AS served_households,
  (
    SELECT row_to_json(json_flow)
    FROM (
	         SELECT t.sys_g_001_001 as value, unit_symbol as unit
           FROM tb_unit u
	         WHERE u.unit_seq = t.sys_g_002_001
         ) as json_flow
  ) AS flow
FROM tb_sistema t
LEFT JOIN tb_pais p ON p.pais_sigla = t.pais_sigla
ORDER BY t.siste_seq
