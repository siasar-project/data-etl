import { Model, DataTypes } from 'sequelize';

module.exports = class Community extends Model {
  static init(sequelize) {
    return super.init({
      siasarId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
      },
      name: DataTypes.STRING,
      surveyDate: DataTypes.DATEONLY,
      surveyYear: DataTypes.INTEGER,
      surveyMonth: DataTypes.INTEGER,
      country: DataTypes.STRING(2),
      adm0: {
        field: 'adm_0',
        type: DataTypes.STRING,
      },
      adm1: {
        field: 'adm_1',
        type: DataTypes.STRING,
      },
      adm2: {
        field: 'adm_2',
        type: DataTypes.STRING,
      },
      adm3: {
        field: 'adm_3',
        type: DataTypes.STRING,
      },
      adm4: {
        field: 'adm_4',
        type: DataTypes.STRING,
      },
      longitude: DataTypes.DOUBLE,
      latitude: DataTypes.DOUBLE,
      geom: DataTypes.GEOMETRY('POINT', 4326),
      score: DataTypes.STRING(1),
      siasarVersion: DataTypes.STRING,
      pictureUrl: DataTypes.STRING,
      population: DataTypes.INTEGER,
      households: DataTypes.INTEGER,
      servedHouseholds: DataTypes.INTEGER,
      householdsWithoutWater: DataTypes.INTEGER,
      healthCentersCount: DataTypes.INTEGER,
      schoolsCount: DataTypes.INTEGER,
      wsp: DataTypes.FLOAT,
      wshl: DataTypes.FLOAT,
      wssi: DataTypes.FLOAT,
      wsl: DataTypes.FLOAT,
      shl: DataTypes.FLOAT,
      wsi: DataTypes.FLOAT,
      sep: DataTypes.FLOAT,
      wslAcc: DataTypes.FLOAT,
      wslCon: DataTypes.FLOAT,
      wslSea: DataTypes.FLOAT,
      wslQua: DataTypes.FLOAT,
      shlSsl: DataTypes.FLOAT,
      shlPer: DataTypes.FLOAT,
      shlWat: DataTypes.FLOAT,
      shlCom: DataTypes.FLOAT,
      wsiAut: DataTypes.FLOAT,
      wsiInf: DataTypes.FLOAT,
      wsiPro: DataTypes.FLOAT,
      wsiTre: DataTypes.FLOAT,
      sepOrg: DataTypes.FLOAT,
      sepOpm: DataTypes.FLOAT,
      sepEco: DataTypes.FLOAT,
      sepEnv: DataTypes.FLOAT,
      ecs: DataTypes.FLOAT,
      ecsEag: DataTypes.FLOAT,
      ecsCag: DataTypes.FLOAT,
      ecsShe: DataTypes.FLOAT,
      ecsShs: DataTypes.FLOAT,
      fis: DataTypes.FLOAT,
      fps: DataTypes.FLOAT,
      fhp: DataTypes.FLOAT,
      fus: DataTypes.FLOAT,
      ftb: DataTypes.FLOAT,
      fafl: DataTypes.FLOAT,
      wco: DataTypes.FLOAT,
      accf: DataTypes.FLOAT,
      householdsConSum: DataTypes.FLOAT,
      householdsSum: DataTypes.FLOAT,
      householdsQuaSum: DataTypes.FLOAT,
    }, {
      sequelize,
      underscored: true,
      indexes: [
        { fields: ['name'] },
        { fields: ['survey_date'] },
        { fields: ['country'] },
        { fields: ['adm_0'] },
        { fields: ['adm_1'] },
        { fields: ['adm_2'] },
        { fields: ['adm_3'] },
        { fields: ['score'] },
        { fields: ['siasar_version'] },
        { fields: ['geom'], method: 'GIST' },
      ],
    });
  }

  static associate(models) {
    this.belongsToMany(models.System, {
      through: models.SystemCommunity,
      foreignKey: 'community_id',
      otherKey: 'system_id',
      as: 'systems',
      onDelete: 'cascade',
    });

    this.belongsToMany(models.ServiceProvider, {
      through: models.ServiceProviderCommunity,
      foreignKey: 'community_id',
      otherKey: 'service_provider_id',
      as: 'service_providers',
      onDelete: 'cascade',
    });

    this.belongsToMany(models.TechnicalProvider, {
      through: models.TechnicalProviderCommunity,
      foreignKey: 'community_id',
      otherKey: 'technical_provider_id',
      as: 'technical_providers',
      onDelete: 'cascade',
    });
  }
};
