import { Model, DataTypes } from 'sequelize';

module.exports = class ServiceProviderCommunity extends Model {
  static init(sequelize) {
    return super.init({
      serviceProviderId: {
        type: DataTypes.INTEGER,
        unique: 'unique',
      },
      communityId: {
        type: DataTypes.INTEGER,
        unique: 'unique',
      },
      servedHouseholds: DataTypes.INTEGER,
    }, {
      sequelize,
      underscored: true,
    });
  }
};
