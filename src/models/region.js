import { Model, DataTypes } from 'sequelize';

module.exports = class Region extends Model {
  static init(sequelize) {
    return super.init({
      name: DataTypes.STRING,
      country: DataTypes.STRING(2),
      admLevel: DataTypes.INTEGER,
      geom: DataTypes.GEOMETRY('MULTIPOLYGON', 4326),
      score: DataTypes.STRING(1),
      wsp: DataTypes.FLOAT,
    }, {
      sequelize,
      underscored: true,
      timestamps: false,
      indexes: [
        { fields: ['name'] },
        { fields: ['country'] },
        { fields: ['adm_level'] },
        { fields: ['score'] },
        { fields: ['geom'], method: 'GIST' },
      ],
    });
  }
};
