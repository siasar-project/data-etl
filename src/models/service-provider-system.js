import { Model, DataTypes } from 'sequelize';

module.exports = class ServiceProviderSystem extends Model {
  static init(sequelize) {
    return super.init({
      serviceProviderId: {
        type: DataTypes.INTEGER,
        unique: 'unique',
      },
      systemId: {
        type: DataTypes.INTEGER,
        unique: 'unique',
      },
      servedHouseholds: DataTypes.INTEGER,
    }, {
      sequelize,
      underscored: true,
    });
  }
};
