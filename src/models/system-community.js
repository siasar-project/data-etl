import { Model, DataTypes } from 'sequelize';

module.exports = class SystemCommunity extends Model {
  static init(sequelize) {
    return super.init({
      systemId: {
        type: DataTypes.INTEGER,
        unique: 'unique',
      },
      communityId: {
        type: DataTypes.INTEGER,
        unique: 'unique',
      },
      servedHouseholds: DataTypes.INTEGER,
    }, {
      sequelize,
      underscored: true,
    });
  }
};
