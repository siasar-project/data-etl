import { Model, DataTypes } from 'sequelize';

module.exports = class TechnicalProviderCommunity extends Model {
  static init(sequelize) {
    return super.init({
      technicalProviderId: {
        type: DataTypes.INTEGER,
        unique: 'unique',
      },
      communityId: {
        type: DataTypes.INTEGER,
        unique: 'unique',
      },
      servedHouseholds: DataTypes.INTEGER,
    }, {
      sequelize,
      underscored: true,
    });
  }
};
