import { Model, DataTypes } from 'sequelize';

module.exports = class TechnicalProvider extends Model {
  static init(sequelize) {
    return super.init({
      siasarId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
      },
      name: DataTypes.STRING,
      surveyDate: DataTypes.DATEONLY,
      surveyYear: DataTypes.INTEGER,
      surveyMonth: DataTypes.INTEGER,
      country: DataTypes.STRING(2),
      adm0: {
        field: 'adm_0',
        type: DataTypes.STRING,
      },
      adm1: {
        field: 'adm_1',
        type: DataTypes.STRING,
      },
      adm2: {
        field: 'adm_2',
        type: DataTypes.STRING,
      },
      adm3: {
        field: 'adm_3',
        type: DataTypes.STRING,
      },
      adm4: {
        field: 'adm_4',
        type: DataTypes.STRING,
      },
      longitude: DataTypes.DOUBLE,
      latitude: DataTypes.DOUBLE,
      geom: DataTypes.GEOMETRY('POINT', 4326),
      score: DataTypes.STRING(1),
      siasarVersion: DataTypes.STRING,
      pictureUrl: DataTypes.STRING,
      servedHouseholds: DataTypes.INTEGER,
      tap: DataTypes.FLOAT,
      tapIs: DataTypes.FLOAT,
      tapIca: DataTypes.FLOAT,
      tapCco: DataTypes.FLOAT,
      tapAin: DataTypes.FLOAT,
      providerType: DataTypes.STRING,
      transportEquipment: DataTypes.STRING(1),
      equipmentMeasuring: DataTypes.STRING(1),
      computerEquipment: DataTypes.STRING(1),
      travelExpenses: DataTypes.STRING(1),
      fuel: DataTypes.STRING(1),
      internetServices: DataTypes.STRING(1),
      techniciansCount: DataTypes.INTEGER,
      annualBudget: DataTypes.BOOLEAN,
      budget: DataTypes.JSON,
    }, {
      sequelize,
      underscored: true,
      indexes: [
        { fields: ['name'] },
        { fields: ['survey_date'] },
        { fields: ['country'] },
        { fields: ['adm_0'] },
        { fields: ['adm_1'] },
        { fields: ['adm_2'] },
        { fields: ['adm_3'] },
        { fields: ['score'] },
        { fields: ['siasar_version'] },
        { fields: ['geom'], method: 'GIST' },
      ],
    });
  }

  static associate(models) {
    this.belongsToMany(models.Community, {
      through: models.TechnicalProviderCommunity,
      foreignKey: 'technical_provider_id',
      otherKey: 'community_id',
      as: 'communities',
      onDelete: 'cascade',
    });
  }
};
