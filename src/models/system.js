import { Model, DataTypes } from 'sequelize';

module.exports = class System extends Model {
  static init(sequelize) {
    return super.init({
      siasarId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
      },
      name: DataTypes.STRING,
      surveyDate: DataTypes.DATEONLY,
      surveyYear: DataTypes.INTEGER,
      surveyMonth: DataTypes.INTEGER,
      country: DataTypes.STRING(2),
      adm0: {
        field: 'adm_0',
        type: DataTypes.STRING,
      },
      adm1: {
        field: 'adm_1',
        type: DataTypes.STRING,
      },
      adm2: {
        field: 'adm_2',
        type: DataTypes.STRING,
      },
      adm3: {
        field: 'adm_3',
        type: DataTypes.STRING,
      },
      adm4: {
        field: 'adm_4',
        type: DataTypes.STRING,
      },
      longitude: DataTypes.DOUBLE,
      latitude: DataTypes.DOUBLE,
      geom: DataTypes.GEOMETRY('POINT', 4326),
      score: DataTypes.STRING(1),
      siasarVersion: DataTypes.STRING,
      sketchUrl: DataTypes.STRING,
      pictureUrl: DataTypes.STRING,
      buildingYear: DataTypes.INTEGER,
      servedHouseholds: DataTypes.INTEGER,
      supplyType: DataTypes.ARRAY(DataTypes.STRING),
      wsi: DataTypes.FLOAT,
      wsiAut: DataTypes.FLOAT,
      wsiInf: DataTypes.FLOAT,
      wsiPro: DataTypes.FLOAT,
      wsiTre: DataTypes.FLOAT,
      catf: DataTypes.FLOAT,
      traf: DataTypes.FLOAT,
      stof: DataTypes.FLOAT,
      disf: DataTypes.FLOAT,
      treatSust: DataTypes.FLOAT,
      treatPat: DataTypes.FLOAT,
      chlora: DataTypes.BOOLEAN,
      flow: DataTypes.JSON,
    }, {
      sequelize,
      underscored: true,
      indexes: [
        { fields: ['name'] },
        { fields: ['survey_date'] },
        { fields: ['country'] },
        { fields: ['adm_0'] },
        { fields: ['adm_1'] },
        { fields: ['adm_2'] },
        { fields: ['adm_3'] },
        { fields: ['score'] },
        { fields: ['siasar_version'] },
        { fields: ['geom'], method: 'GIST' },
        { fields: ['supply_type'], method: 'GIN' },
      ],
    });
  }

  static associate(models) {
    this.belongsToMany(models.Community, {
      through: models.SystemCommunity,
      foreignKey: 'system_id',
      otherKey: 'community_id',
      as: 'communities',
      onDelete: 'cascade',
    });

    this.belongsToMany(models.ServiceProvider, {
      through: models.ServiceProviderSystem,
      foreignKey: 'system_id',
      otherKey: 'service_provider_id',
      as: 'service_providers',
      onDelete: 'cascade',
    });
  }
};
