const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const config = require('../config/config.js');

const env = process.env.NODE_ENV || 'development';

const configData = config[env];
const configBI = config[env].bi;

const data = new Sequelize(configData.database, configData.username, configData.password, configData);
const bi = new Sequelize(configBI.database, configBI.username, configBI.password, configBI);

const models = Object.assign({}, ...fs.readdirSync(__dirname)
  .filter(file => (file.indexOf('.') !== 0 && file !== 'index.js'))
  .map((file) => {
    const model = require(path.join(__dirname, file)); // eslint-disable-line
    return { [model.name]: model.init(data) };
  }));

Object.keys(models).forEach(model => (
  typeof models[model].associate === 'function' && models[model].associate(models)
));

module.exports = {
  models, data, bi, Sequelize,
};
