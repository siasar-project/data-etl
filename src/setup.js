import { data } from './models';

console.log('Creating tables...');

data.sync({ force: true })
  .then(() => {
    data.close().then(() => {
      console.log('Database setup successfully finished.');
    });
  })
  .catch(console.error);
