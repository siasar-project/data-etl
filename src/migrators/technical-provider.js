import Migrator from './migrator';

class TechnicalProviderMigrator extends Migrator {
  constructor() {
    super('technical_provider', 'tb_pat', 'TechnicalProvider');
    this.variables = {
      pat: 'tap',
      patsin: 'tapIs',
      patcap: 'tapIca',
      patcob: 'tapCco',
      patint: 'tapAin',
    };
  }

  parseRecord(record) {
    super.parseRecord(record);
    record.providerType = this.parseProviderType(record.provider_type);
    record.servedHouseholds = record.served_households;
    record.techniciansCount = record.technicians_count;
    record.annualBudget = record.annual_budget;
    record.transportEquipment = this.parseTaxonomy(record.transport_equipment);
    record.equipmentMeasuring = this.parseTaxonomy(record.equipment_measuring);
    record.computerEquipment = this.parseTaxonomy(record.computer_equipment);
    record.travelExpenses = this.parseTaxonomy(record.travel_expenses);
    record.fuel = this.parseTaxonomy(record.fuel);
    record.internetServices = this.parseTaxonomy(record.internet_services);
    record.score = record.score || Migrator.calculateScore(record.tap);
    record.tap = record.tap || Migrator.calculateValue(record.score);
  }

  parseTaxonomy(taxonomy) {
    switch (taxonomy) {
      case 32056:
        return 'A';
      case 32057:
        return 'B';
      case 32058:
        return 'C';
      default:
        return 'D';
    }
  }

  parseProviderType(providerType) {
    switch (providerType) {
      case 'Gobierno Central':
        return 'centralGovernment';
      case 'Gobierno Municipal':
        return 'localGovernment';
      case 'ONG':
        return 'ngo';
      default:
        return 'other';
    }
  }
}

const migrator = new TechnicalProviderMigrator();
migrator.migrate();
