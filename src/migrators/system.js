import Migrator from './migrator';

class SystemMigrator extends Migrator {
  constructor() {
    super('system', 'tb_sistema', 'System');
    this.variables = {
      eia: 'wsi',
      eiaaut: 'wsiAut',
      eiainf: 'wsiInf',
      eiazpa: 'wsiPro',
      eiastr: 'wsiTre',
      fcap: 'catf',
      fcon: 'traf',
      falm: 'stof',
      fdis: 'disf',
      trat_sol: 'treatSust',
      trat_pat: 'treatPat',
    };
  }

  parseRecord(record) {
    super.parseRecord(record);
    record.buildingYear = record.building_year;
    record.servedHouseholds = record.served_households;
    if (record.supply_type) {
      record.supplyType = this.parseSupplySystem(record.supply_type);
    }
    record.sketchUrl = record.sketch_url;
    record.chlora = record.chlora === 1;
    record.score = record.score || Migrator.calculateScore(record.wsi);
    record.wsi = record.wsi || Migrator.calculateValue(record.score);
  }

  parseSupplySystem(supplyType) {
    return supplyType.map((value) => {
      switch (value) {
        case 'Acueducto por Gravedad':
          return 'gravitySupply';
        case 'Pozo con Bomba Manual':
          return 'handPump';
        case 'Acueducto por Bombeo':
          return 'pumpedSupply';
        case 'Captación de agua lluvia':
          return 'rainwaterHarvesting';
        default:
          return 'other';
      }
    });
  }

  async postMigrate() {
    const query = `
      SELECT
        comun_seq as "community_id",
        siste_seq as "system_id",
        SUM(grpdo_num_domic_atend) as "served_households"
      FROM tb_grp_domicilio
      WHERE siste_seq IS NOT NULL
        AND comun_seq IS NOT NULL
        AND siste_seq in (
          SELECT siste_seq
          FROM tb_sistema
        )
        AND comun_seq in (
          SELECT comun_seq
          FROM tb_comunidade
        )
      GROUP BY comun_seq, siste_seq
    `;
    const result = await this.biQuery(query, { type: this.bi.QueryTypes.SELECT });
    console.log(`Inserting ${result.length} relations with Community`);
    await this.models.SystemCommunity.bulkCreate(result);
  }
}

const migrator = new SystemMigrator();
migrator.migrate();
