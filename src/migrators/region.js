import stamp from 'console-stamp';
import moment from 'moment';

import { models, data } from '../models/index';

class RegionMigrator {
  constructor() {
    stamp(console, {
      formatter: () => {
        const duration = moment.duration(process.uptime(), 's');
        let time;
        if (duration.asSeconds() < 60) {
          time = `  ${duration.asSeconds().toFixed(2)}s`;
        } else {
          time = `  ${duration.asMinutes().toFixed(2)}m`;
        }
        return `${moment().format('DD/MM/YY HH:MM:ss.SSS')} | ${time.slice(-6)}`;
      },
      colors: {
        stamp: 'yellow',
        label: 'white',
        metadata: 'green',
      },
      metadata: () => `  ${(Math.round(process.memoryUsage().rss / 1024 / 1024))}MB`.slice(-6),
    });
  }

  async migrate() {
    console.log('Migration of Region intialized');

    try {
      console.log('Truncating table');
      await models.Region.destroy({ truncate: true, cascade: true });

      console.log('Inserting records');
      await data.query(`
        INSERT INTO regions (country, adm_level, name, geom)
        SELECT country, 1, name, geom FROM base.adm_1
        UNION
        SELECT country, 2, name, geom FROM base.adm_2
      `, { type: data.QueryTypes.INSERT });
    } catch (error) {
      console.error(error);
    } finally {
      data.close();
      console.log('Migration finished');
    }
  }
}

const migrator = new RegionMigrator();
migrator.migrate();
