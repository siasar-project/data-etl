import Migrator from './migrator';

class ServiceProviderMigrator extends Migrator {
  constructor() {
    super('service_provider', 'tb_prest_servico', 'ServiceProvider');
    this.variables = {
      pse: 'sep',
      psegor: 'sepOrg',
      psegom: 'sepOpm',
      psegef: 'sepEco',
      psegam: 'sepEnv',
      rent: 'prof',
      fgps: 'amsp',
      foperps: 'opefsp',
      fgenps: 'genfsp',
      ftransps: 'ramafps',
      foem: 'omf',
      fclres: 'resclf',
      freg: 'regf',
      fmicro: 'microf',
      fref: 'berf',
      frec: 'cerf',
      frent: 'pro',
      frl: 'lrf',
      frs: 'srf',
      frcsd: 'dscrf',
      fatecor: 'espf',
      fatepre: 'paaf',
    };
  }

  parseRecord(record) {
    super.parseRecord(record);
    record.providerType = record.provider_type;
    record.monthBilling = record.month_billing;
    record.legalStatus = record.legal_status === 'Está legalizado';
    record.servedHouseholds = record.served_households;
    record.womenCount = record.women_count;
    record.score = record.score || Migrator.calculateScore(record.sep);
    record.sep = record.sep || Migrator.calculateValue(record.score);
    record.profitable = this.parseProfitability(record.prof);
    record.preventiveMaintenance = (record.preventive_maintenance === 32047 || record.preventive_maintenance === 32048);
  }

  parseProfitability(profitable) {
    switch (true) {
      case (profitable > 1):
        return true;
      case (profitable > 0 || profitable < 1):
        return false;
      default:
        return null;
    }
  }

  async postMigrate() {
    let query = `
      SELECT
        prser_seq as "service_provider_id",
        comun_seq as "community_id",
        SUM(grpdo_num_domic_atend) as "served_households"
      FROM tb_grp_domicilio
      WHERE prser_seq IS NOT NULL
        AND comun_seq IS NOT NULL
        AND prser_seq in (
          SELECT prser_seq
          FROM tb_prest_servico
        )
        AND comun_seq in (
          SELECT comun_seq
          FROM tb_comunidade
        )
      GROUP BY comun_seq, prser_seq
    `;
    let result = await this.biQuery(query, { type: this.bi.QueryTypes.SELECT });
    console.log(`Inserting ${result.length} relations with Community`);
    await this.models.ServiceProviderCommunity.bulkCreate(result);

    query = `
      SELECT
      prser_seq as "service_provider_id",
        siste_seq as "system_id",
        SUM(grpdo_num_domic_atend) as "served_households"
      FROM tb_grp_domicilio
      WHERE prser_seq IS NOT NULL
        AND siste_seq IS NOT NULL
        AND prser_seq in (
          SELECT prser_seq
          FROM tb_prest_servico
        )
        AND siste_seq in (
          SELECT siste_seq
          FROM tb_sistema
        )
      GROUP BY siste_seq, prser_seq
    `;
    result = await this.biQuery(query, { type: this.bi.QueryTypes.SELECT });
    console.log(`Inserting ${result.length} relations with System`);
    await this.models.ServiceProviderSystem.bulkCreate(result);
  }
}

const migrator = new ServiceProviderMigrator();
migrator.migrate();
