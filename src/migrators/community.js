import Migrator from './migrator';

class CommunityMigrator extends Migrator {
  constructor() {
    super('community', 'tb_comunidade', 'Community');
    this.variables = {
      ias: 'wsp',
      nash: 'wshl',
      issa: 'wssi',
      nsa: 'wsl',
      nsh: 'shl',
      eiacom: 'wsi',
      psecom: 'sep',
      nsaacc: 'wslAcc',
      nsacon: 'wslCon',
      nsaest: 'wslSea',
      nsacal: 'wslQua',
      nshnss: 'shlSsl',
      nshhpe: 'shlPer',
      nshhho: 'shlWat',
      nshhco: 'shlCom',
      eiaaut: 'wsiAut',
      eiainf: 'wsiInf',
      eiazpa: 'wsiPro',
      eiastr: 'wsiTre',
      psegor: 'sepOrg',
      psegom: 'sepOpm',
      psegef: 'sepEco',
      psegam: 'sepEnv',
      ecs: 'ecs',
      ecseag: 'ecsEag',
      ecscag: 'ecsCag',
      ecsshe: 'ecsShe',
      ecsshs: 'ecsShs',
      fis: 'fis',
      fps: 'fps',
      fhp: 'fhp',
      fus: 'fus',
      ftb: 'ftb',
      fafl: 'fafl',
      cagua: 'wco',
      facc: 'accf',
      sum_viv_ser_con: 'householdsConSum',
      sum_viv_ser: 'householdsSum',
      sum_viv_ser_cal: 'householdsQuaSum',
    };
  }

  parseRecord(record) {
    super.parseRecord(record);
    record.score = record.score || Migrator.calculateScore(record.wsp);
    record.wsp = record.wsp || Migrator.calculateValue(record.score);
    record.healthCentersCount = record.health_centers_count;
    record.schoolsCount = record.schools_count;
    record.householdsWithoutWater = record.households_without_water;
    record.servedHouseholds = record.served_households;
  }

  async postMigrate() {
    console.log('Updating regions');

    let query = `
      UPDATE regions
      SET wsp = (
        SELECT avg(communities.wsp)
        FROM communities
        WHERE ST_Within(communities.geom, regions.geom)
      )
    `;
    await this.dataQuery(query, { type: this.data.QueryTypes.UPDATE });

    query = `
      UPDATE regions
      SET score = (
        CASE
          WHEN wsp >= 0 AND wsp <= 0.4 then 'D'
          WHEN wsp > 0.4 AND wsp <= 0.7 then 'C'
          WHEN wsp > 0.7 AND wsp <= 0.9 then 'B'
          WHEN wsp > 0.9 then 'A'
        END
      )
    `;
    await this.dataQuery(query, { type: this.data.QueryTypes.UPDATE });
  }
}

const migrator = new CommunityMigrator();
migrator.migrate();
